<?php

declare(strict_types=1);

namespace Omnevo\MonoAkeneoBundle\Service;

use Akeneo\Pim\Enrichment\Component\Category\Connector\ArrayConverter\FlatToStandard\Category;

/**
 * Just a fancy service using some akeneo classes
 *
 * @package Omnevo\MonoAkeneoBundle\Service
 */
class FancyService extends Category
{
  public function convert(array $item, array $options = [])
  {
    return parent::convert($item, $options);
  }
}
