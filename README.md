# Omnevo Akeneo Monorepo Playground

This is a sample project using [simplify/monorepo-builder](https://github.com/symplify/monorepo-builder) to be able to play around with some sample akeneo projects, some bundles used by the projects etc.

Read also more on mono repositories for php projects on [All You Always Wanted to Know About Monorepo But Were Afraid To Ask](https://tomasvotruba.com/blog/2019/10/28/all-you-always-wanted-to-know-about-monorepo-but-were-afraid-to-ask/#what-is-monorepo) or why to use a mono repository on [Advantages of monorepos
](https://danluu.com/monorepo/). Or just read what the big ones like Google or Facebook are doing (or don't).

## Repository Structure

The repository structure is taken from the structuring as it is done by nx - in the end we may also add nx on top of this monorepo structure, as `simplify/monorepo-builder` can mainly be used to

- synchronize dependencies
- provide a release flow together with some additional handling of composer dependencies

### The structure

```txt
- apps
  - some-app
  - other-app
- libs
  - some-lib
  - other-lib
- tools
  - generator
    - some-generator-tool
```

- apps  
  Contains the applications of the mono repository
- libs  
  Contains the libraries of the mono repository
- tools  
  Contains some additional tooling